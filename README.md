# Portfolio

My portfolio website

## Development
Run the development server using
```shell
npm run dev
```

## Production build
To build the project, run
```shell
npm run build
```
